CREATE TABLE tx_oauth2server_domain_model_accesstoken (
    token varchar(255) DEFAULT '' NOT NULL,
    expiry varchar(255) DEFAULT '' NOT NULL,
    user int(11) DEFAULT 0 NOT NULL,
    client int(11) DEFAULT 0 NOT NULL,
    revoked int(11) DEFAULT 0 NOT NULL
);
CREATE TABLE tx_oauth2server_domain_model_authcode (
    code varchar(255) DEFAULT '' NOT NULL,
    expiry varchar(255) DEFAULT '' NOT NULL,
    user int(11) DEFAULT 0 NOT NULL,
    client int(11) DEFAULT 0 NOT NULL,
    revoked int(11) DEFAULT 0 NOT NULL
);
CREATE TABLE tx_oauth2server_domain_model_client (
    auth_id varchar(255) DEFAULT '' NOT NULL,
    auth_secret varchar(255) DEFAULT '' NOT NULL,
    name varchar(255) DEFAULT '' NOT NULL,
    redirect_uri varchar(255) DEFAULT '' NOT NULL,
    confidential int(11) DEFAULT 0 NOT NULL,
    scopes tinytext DEFAULT '' NOT NULL
);
CREATE TABLE tx_oauth2server_domain_model_refreshtoken (
    token varchar(255) DEFAULT '' NOT NULL,
    expiry varchar(255) DEFAULT '' NOT NULL,
    access_token int(11) DEFAULT 0 NOT NULL,
    revoked int(11) DEFAULT 0 NOT NULL
);