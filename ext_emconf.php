<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'OAuth2 Server',
    'description' => 'Use TYPO3 as an OAuth2 Server. Based on https://oauth2.thephpleague.com',
    'version' => '0.0.3',
    'clearcacheonload' => true,
    'author' => 'Marco Huber',
    'author_email' => 'mail@marco-huber.de',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
