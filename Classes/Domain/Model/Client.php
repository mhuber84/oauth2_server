<?php
namespace Mhuber84\Oauth2Server\Domain\Model;

class Client extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * authId
     *
     * @var string
     */
    protected $authId;

    /**
     * authSecret
     *
     * @var string
     */
    protected $authSecret;

    /**
     * name
     *
     * @var string
     */
    protected $name;

    /**
     * redirectUri
     *
     * @var string
     */
    protected $redirectUri;

    /**
     * confidential
     *
     * @var bool
     */
    protected $confidential;

    /**
     * grantTypes
     *
     * @var array
     * @todo grant types should be configurable for each client in the TYPO3 BE
     */
    protected $grantTypes = [
        'password',
        'refresh_token',
    ];

    /**
     * scopes
     *
     * @var array
     * @todo scopes should be configurable for each client in the TYPO3 BE
     */
    protected $scopes = [
        'basic',
        'email',
    ];

    /**
     * @return string
     */
    public function getAuthId()
    {
        return $this->authId;
    }

    /**
     * @param string $authId
     */
    public function setAuthId($authId)
    {
        $this->authId = $authId;
    }

    /**
     * @return string
     */
    public function getAuthSecret()
    {
        return $this->authSecret;
    }

    /**
     * @param string $authSecret
     */
    public function setAuthSecret($authSecret)
    {
        $this->authSecret = $authSecret;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRedirectUri()
    {
        return $this->redirectUri;
    }

    /**
     * @param string $redirectUri
     */
    public function setRedirectUri($redirectUri)
    {
        $this->redirectUri = $redirectUri;
    }

    /**
     * @return bool
     */
    public function isConfidential()
    {
        return $this->confidential;
    }

    /**
     * @return bool
     */
    public function getConfidential()
    {
        return $this->confidential;
    }

    /**
     * @param bool $confidential
     */
    public function setConfidential($confidential)
    {
        $this->confidential = $confidential;
    }

    /**
     * @return array
     */
    public function getGrantTypes()
    {
        return $this->grantTypes;
    }

    /**
     * @param array $grantTypes
     */
    public function setGrantTypes($grantTypes)
    {
        $this->grantTypes = $grantTypes;
    }

    /**
     * @param string $grantType
     */
    public function addGrantType($grantType)
    {
        if(!in_array($grantType, $this->grantTypes)){
            $this->grantTypes[] = $grantType;
        }
    }

    /**
     * @param string $grantType
     */
    public function removeGrantType($grantType)
    {
        if(in_array($grantType, $this->grantTypes)){
            $this->grantTypes = array_diff($this->grantTypes, [$grantType]);
        }
    }

    /**
     * @return array
     */
    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param array $scopes
     */
    public function setScopes($scopes)
    {
        $this->scopes = $scopes;
    }

    /**
     * @param string $scope
     */
    public function addScope($scope)
    {
        if(!in_array($scope, $this->scopes)){
            $this->scopes[] = $scope;
        }
    }

    /**
     * @param string $scope
     */
    public function removeScope($scope)
    {
        if(in_array($scope, $this->scopes)){
            $this->scopes = array_diff($this->scopes, [$scope]);
        }
    }

}