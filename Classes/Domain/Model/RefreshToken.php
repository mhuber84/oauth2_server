<?php
namespace Mhuber84\Oauth2Server\Domain\Model;

class RefreshToken extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

    /**
     * @var string
     */
    protected $token;

    /**
     * @var \DateTime
     */
    protected $expiry;

    /**
     * @var \Mhuber84\Oauth2Server\Domain\Model\AccessToken
     */
    protected $accessToken;

    /**
     * @var bool
     */
    protected $revoked = false;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return \DateTime
     */
    public function getExpiry()
    {
        return $this->expiry;
    }

    /**
     * @param \DateTime $expiry
     */
    public function setExpiry($expiry)
    {
        $this->expiry = $expiry;
    }

    /**
     * @return AccessToken
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param AccessToken $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return bool
     */
    public function isRevoked()
    {
        return $this->revoked;
    }

    /**
     * @return bool
     */
    public function getRevoked()
    {
        return $this->revoked;
    }

    /**
     * @param bool $revoked
     */
    public function setRevoked($revoked)
    {
        $this->revoked = $revoked;
    }
}