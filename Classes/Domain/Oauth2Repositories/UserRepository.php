<?php
namespace Mhuber84\Oauth2Server\Domain\Oauth2Repositories;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Mhuber84\Oauth2Server\Domain\Oauth2Entities\UserEntity;
use TYPO3\CMS\Core\Authentication\AuthenticationService;
use TYPO3\CMS\Core\Authentication\LoginType;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

class UserRepository implements UserRepositoryInterface {

    /**
     * {@inheritdoc}
     */
    public function getUserEntityByUserCredentials(
        $username,
        $password,
        $grantType,
        ClientEntityInterface $clientEntity
    ) {
        $authenticated = false;

        // Use 'auth' service to find the user
        // First found user will be used
        $subType = 'getUserFE';
        $loginData = [
            'status' => LoginType::LOGIN,
            'uname' => $username,
            'uident_text' => $password,
        ];
        /** @var FrontendUserAuthentication $typo3UserAuthentication */
        $typo3UserAuthentication = GeneralUtility::makeInstance(FrontendUserAuthentication::class);
        $typo3UserAuthentication->checkPid = false;
        $authInfo = $typo3UserAuthentication->getAuthInfoArray();
        $authServices = $this->getAuthServices($subType, $loginData, $authInfo, $typo3UserAuthentication);
        /** @var AuthenticationService $serviceObj */
        foreach ($authServices as $serviceObj) {
            if ($row = $serviceObj->getUser()) {
                break;
            }
        }
        if(strtolower($username) == strtolower($row[$authInfo['db_user']['username_column']])) {
            $subType = 'authUserFE';
            $authServices = $this->getAuthServices($subType, $loginData, $authInfo, $typo3UserAuthentication);
            /** @var AuthenticationService $serviceObj */
            foreach ($authServices as $serviceObj) {
                if (($ret = $serviceObj->authUser($row)) > 0) {
                    // If the service returns >=200 then no more checking is needed - useful for IP checking without password
                    if ((int)$ret >= 200) {
                        $authenticated = true;
                        break;
                    }
                    if ((int)$ret >= 100) {
                    } else {
                        $authenticated = true;
                    }
                } else {
                    $authenticated = false;
                    break;
                }
            }

            if ($authenticated) {
                return new UserEntity($row[$authInfo['db_user']['userid_column']]);
            }
        }

        return;
    }

    /**
     * Initializes authentication services to be used in a foreach loop
     *
     * @param string $subType e.g. getUserFE
     * @param array $loginData
     * @param array $authInfo
     * @param FrontendUserAuthentication $typo3UserAuthentication
     * @return \Traversable A generator of service objects
     */
    protected function getAuthServices(string $subType, array $loginData, array $authInfo, FrontendUserAuthentication $typo3UserAuthentication): \Traversable
    {
        $serviceChain = '';
        /** @var AuthenticationService $serviceObj */
        while (is_object($serviceObj = GeneralUtility::makeInstanceService('auth', $subType, $serviceChain))) {
            $serviceChain .= ',' . $serviceObj->getServiceKey();
            $serviceObj->initAuth($subType, $loginData, $authInfo, $typo3UserAuthentication);
            yield $serviceObj;
        }
    }
}
