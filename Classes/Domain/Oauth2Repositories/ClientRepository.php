<?php
namespace Mhuber84\Oauth2Server\Domain\Oauth2Repositories;

use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Mhuber84\Oauth2Server\Domain\Oauth2Entities\ClientEntity;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashFactory;
use TYPO3\CMS\Core\Crypto\PasswordHashing\PasswordHashInterface;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ClientRepository implements ClientRepositoryInterface {

    /**
     * {@inheritdoc}
     */
    public function getClientEntity($clientIdentifier)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_client');
        $typo3Client = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_client')
            ->where(
                $queryBuilder->expr()->eq('auth_id', $queryBuilder->createNamedParameter($clientIdentifier, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();

        // Check if client is registered
        if ($typo3Client == false) {
            return;
        }

        $client = new ClientEntity();

        $client->setIdentifier($clientIdentifier);
        $client->setName($typo3Client['name']);
        $client->setRedirectUri($typo3Client['redirect_uri']);
        $client->setConfidential((bool) $typo3Client['confidential']);
        $client->setScopes(GeneralUtility::trimExplode(',', $typo3Client['scopes']));

        return $client;
    }

    /**
     * {@inheritdoc}
     */
    public function validateClient($clientIdentifier, $clientSecret, $grantType)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_client');
        $typo3Client = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_client')
            ->where(
                $queryBuilder->expr()->eq('auth_id', $queryBuilder->createNamedParameter($clientIdentifier, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();

        // Check if client is registered
        if ($typo3Client == false) {
            return false;
        }

        // Check if grant type is allowed for client
        /* @todo safe grant type per client
        if (in_array($grantType, $typo3Client['grant_types']) == false) {
            return false;
        }
        */

        /** @var PasswordHashFactory $saltFactory */
        $saltFactory = GeneralUtility::makeInstance(PasswordHashFactory::class);
        /** @var PasswordHashInterface $hashInstance */
        $hashInstance = $saltFactory->get($typo3Client['auth_secret'], 'BE');
        $isValidPassword = $hashInstance->checkPassword($clientSecret, $typo3Client['auth_secret']);

        return !(((bool) $typo3Client['confidential']) === true && $isValidPassword === false);
    }
}