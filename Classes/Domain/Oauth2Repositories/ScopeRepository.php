<?php
namespace Mhuber84\Oauth2Server\Domain\Oauth2Repositories;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;
use Mhuber84\Oauth2Server\Domain\Oauth2Entities\ScopeEntity;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ScopeRepository implements ScopeRepositoryInterface {

    /**
     * @var array
     */
    protected $scopes;

    /**
     * @param array $scopes
     */
    public function __construct($scopes)
    {
        $this->scopes = $scopes;
    }

    /**
     * {@inheritdoc}
     */
    public function getScopeEntityByIdentifier($scopeIdentifier)
    {
        if (\array_key_exists($scopeIdentifier, $this->scopes) === false) {
            return;
        }

        $scope = new ScopeEntity();
        $scope->setIdentifier($scopeIdentifier);

        return $scope;
    }

    /**
     * {@inheritdoc}
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    ) {
        /*
        // Only scopes allowed for the client
        $clientScopes = $clientEntity->getScopes();
        $scopes = array_intersect_assoc($scopes, $this->scopes, $clientScopes);
        */

        /*
        // Example of programatically modifying the final scope of the access token
        if ((int) $userIdentifier === 1) {
            $scope = new ScopeEntity();
            $scope->setIdentifier('email');
            $scopes[] = $scope;
        }
        */

        return $scopes;
    }
}