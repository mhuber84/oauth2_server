<?php
namespace Mhuber84\Oauth2Server\Domain\Oauth2Repositories;

use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use Mhuber84\Oauth2Server\Domain\Oauth2Entities\AccessTokenEntity;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AccessTokenRepository implements AccessTokenRepositoryInterface {

    /**
     * {@inheritdoc}
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity)
    {
        // Check if access token already exists
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_accesstoken');
        $typo3AccessToken = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_accesstoken')
            ->where(
                $queryBuilder->expr()->eq('token', $queryBuilder->createNamedParameter($accessTokenEntity->getIdentifier(), Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if ($typo3AccessToken !== false) {
            throw OAuthServerException::serverError('Token has to be unique');
            return;
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_client');
        $client = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_client')
            ->where(
                $queryBuilder->expr()->eq('auth_id', $queryBuilder->createNamedParameter($accessTokenEntity->getClient()->getIdentifier(), Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_accesstoken');
        $queryBuilder
            ->insert('tx_oauth2server_domain_model_accesstoken')
            ->values([
                'token' => $accessTokenEntity->getIdentifier(),
                'expiry' => $accessTokenEntity->getExpiryDateTime()->getTimestamp(),
                'user' => $accessTokenEntity->getUserIdentifier(),
                'client' => $client['uid'],
                'crdate' => $GLOBALS['EXEC_TIME'],
                'tstamp' => $GLOBALS['EXEC_TIME'],
            ])
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function revokeAccessToken($tokenId)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_accesstoken');
        $typo3AccessToken = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_accesstoken')
            ->where(
                $queryBuilder->expr()->eq('token', $queryBuilder->createNamedParameter($tokenId, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if($typo3AccessToken !== false){
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_oauth2server_domain_model_accesstoken');
            $queryBuilder
                ->update('tx_oauth2server_domain_model_accesstoken')
                ->where(
                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($typo3AccessToken['uid'], Connection::PARAM_INT))
                )
                ->set('revoked', true)
                ->set('tstamp', $GLOBALS['EXEC_TIME'])
                ->execute();;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isAccessTokenRevoked($tokenId)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_accesstoken');
        $typo3AccessToken = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_accesstoken')
            ->where(
                $queryBuilder->expr()->eq('token', $queryBuilder->createNamedParameter($tokenId, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if($typo3AccessToken !== false){
            return (bool) $typo3AccessToken['revoked'];
        }
        throw OAuthServerException::serverError('Token not found');
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getNewToken(ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null)
    {
        $accessToken = new AccessTokenEntity();
        $accessToken->setClient($clientEntity);
        foreach ($scopes as $scope) {
            $accessToken->addScope($scope);
        }
        $accessToken->setUserIdentifier($userIdentifier);

        return $accessToken;
    }
}