<?php
namespace Mhuber84\Oauth2Server\Domain\Oauth2Repositories;

use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Mhuber84\Oauth2Server\Domain\Oauth2Entities\RefreshTokenEntity;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class RefreshTokenRepository implements RefreshTokenRepositoryInterface {

    /**
     * {@inheritdoc}
     */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity)
    {
        // Check if access token already exists
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_refreshtoken');
        $typo3RefreshToken = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_refreshtoken')
            ->where(
                $queryBuilder->expr()->eq('token', $queryBuilder->createNamedParameter($refreshTokenEntity->getIdentifier(), Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if ($typo3RefreshToken !== false) {
            throw OAuthServerException::serverError('Token has to be unique');
            return;
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_accesstoken');
        $typo3AccessToken = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_accesstoken')
            ->where(
                $queryBuilder->expr()->eq('token', $queryBuilder->createNamedParameter($refreshTokenEntity->getAccessToken()->getIdentifier(), Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_refreshtoken');
        $queryBuilder
            ->insert('tx_oauth2server_domain_model_refreshtoken')
            ->values([
                'token' => $refreshTokenEntity->getIdentifier(),
                'expiry' => $refreshTokenEntity->getExpiryDateTime()->getTimestamp(),
                'access_token' => $typo3AccessToken['uid'],
                'crdate' => $GLOBALS['EXEC_TIME'],
                'tstamp' => $GLOBALS['EXEC_TIME'],
            ])
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function revokeRefreshToken($tokenId)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_refreshtoken');
        $typo3RefreshToken = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_refreshtoken')
            ->where(
                $queryBuilder->expr()->eq('token', $queryBuilder->createNamedParameter($tokenId, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if($typo3RefreshToken !== false){
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_oauth2server_domain_model_refreshtoken');
            $queryBuilder
                ->update('tx_oauth2server_domain_model_refreshtoken')
                ->where(
                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($typo3RefreshToken['uid'], Connection::PARAM_INT))
                )
                ->set('revoked', true)
                ->set('tstamp', $GLOBALS['EXEC_TIME'])
                ->execute();;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isRefreshTokenRevoked($tokenId)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_refreshtoken');
        $typo3RefreshToken = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_refreshtoken')
            ->where(
                $queryBuilder->expr()->eq('token', $queryBuilder->createNamedParameter($tokenId, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if($typo3RefreshToken !== false){
            return (bool) $typo3RefreshToken['revoked'];
        }
        throw OAuthServerException::serverError('Token not found');
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getNewRefreshToken()
    {
        return new RefreshTokenEntity();
    }
}