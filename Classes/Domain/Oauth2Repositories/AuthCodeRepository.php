<?php
namespace Mhuber84\Oauth2Server\Domain\Oauth2Repositories;

use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use Mhuber84\Oauth2Server\Domain\Oauth2Entities\AuthCodeEntity;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class AuthCodeRepository implements AuthCodeRepositoryInterface {

    /**
     * {@inheritdoc}
     */
    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity)
    {
        // Check if access token already exists
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_authcode');
        $typo3AuthCode = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_authcode')
            ->where(
                $queryBuilder->expr()->eq('code', $queryBuilder->createNamedParameter($authCodeEntity->getIdentifier(), Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if ($typo3AuthCode !== false) {
            throw OAuthServerException::serverError('Code has to be unique');
            return;
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_client');
        $client = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_client')
            ->where(
                $queryBuilder->expr()->eq('auth_id', $queryBuilder->createNamedParameter($authCodeEntity->getClient()->getIdentifier(), Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_authcode');
        $queryBuilder
            ->insert('tx_oauth2server_domain_model_authcode')
            ->values([
                'code' => $authCodeEntity->getIdentifier(),
                'expiry' => $authCodeEntity->getExpiryDateTime()->getTimestamp(),
                'user' => $authCodeEntity->getUserIdentifier(),
                'client' => $client['uid'],
                'crdate' => $GLOBALS['EXEC_TIME'],
                'tstamp' => $GLOBALS['EXEC_TIME'],
            ])
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function revokeAuthCode($codeId)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_authcode');
        $typo3AuthCode = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_authcode')
            ->where(
                $queryBuilder->expr()->eq('code', $queryBuilder->createNamedParameter($codeId, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if($typo3AuthCode !== false){
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
                ->getQueryBuilderForTable('tx_oauth2server_domain_model_authcode');
            $queryBuilder
                ->update('tx_oauth2server_domain_model_authcode')
                ->where(
                    $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($typo3AuthCode['uid'], Connection::PARAM_INT))
                )
                ->set('revoked', true)
                ->set('tstamp', $GLOBALS['EXEC_TIME'])
                ->execute();;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isAuthCodeRevoked($codeId)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_oauth2server_domain_model_authcode');
        $typo3AuthCode = $queryBuilder
            ->select('*')
            ->from('tx_oauth2server_domain_model_authcode')
            ->where(
                $queryBuilder->expr()->eq('code', $queryBuilder->createNamedParameter($codeId, Connection::PARAM_STR))
            )
            ->execute()
            ->fetch();
        if($typo3AuthCode !== false){
            return (bool) $typo3AuthCode['revoked'];
        }
        throw OAuthServerException::serverError('Code not found');
        return;
    }

    /**
     * {@inheritdoc}
     */
    public function getNewAuthCode()
    {
        return new AuthCodeEntity();
    }
}
