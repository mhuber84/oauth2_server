<?php
namespace Mhuber84\Oauth2Server\Controller;

use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use Mhuber84\Oauth2Server\Domain\Oauth2Repositories\AccessTokenRepository;
use Mhuber84\Oauth2Server\Domain\Oauth2Repositories\ClientRepository;
use Mhuber84\Oauth2Server\Domain\Oauth2Repositories\RefreshTokenRepository;
use Mhuber84\Oauth2Server\Domain\Oauth2Repositories\ScopeRepository;
use Mhuber84\Oauth2Server\Domain\Oauth2Repositories\UserRepository;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

class AuthenticationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    /**
curl -X "POST" "https://my.ddev.site/?type=1586350745" \
-H "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "tx_oauth2server_oauth2[clientId]=MY-CLIENT-ID" \
--data-urlencode "tx_oauth2server_oauth2[clientSecret]=MY-CLIENT-SECRET" \
--data-urlencode "tx_oauth2server_oauth2[userName]=MY-USER-NAME" \
--data-urlencode "tx_oauth2server_oauth2[userPassword]=MY-USER-PASSWORD" \
--data-urlencode "tx_oauth2server_oauth2[scope]=MY-SCOPE-1 MY-SCOPE-2"
     *
curl -X "POST" "https://my.ddev.site/oauth2/access_token/get/MY-CLIENT-ID/MY-USER-NAME/MY-SCOPE-1+MY-SCOPE-2/1586350745" \
-H "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "tx_oauth2server_oauth2[clientSecret]=MY-CLIENT-SECRET" \
--data-urlencode "tx_oauth2server_oauth2[userPassword]=MY-USER-PASSWORD"
     *
     * @param string $clientId
     * @param string $clientSecret
     * @param string $userName
     * @param string $userPassword
     * @param string $scope
     */
    public function loginAction($clientId, $clientSecret, $userName, $userPassword, $scope){
        $grantType = 'password';
        $extSettings = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('oauth2_server');
        $pathToPrivateKey = 'file://' . PathUtility::getAbsolutePathOfRelativeReferencedFileOrPath(Environment::getProjectPath(). '/', $extSettings['pathToPrivateKey']);
        $encryptionKey = $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'];
        //var_dump($clientId, $clientSecret, $userName, $userPassword, $scope, $grantType, $pathToPrivateKey, $encryptionKey);

        // Setup the authorization server
        $server = new AuthorizationServer(
            new ClientRepository(),                 // instance of ClientRepositoryInterface
            new AccessTokenRepository(),            // instance of AccessTokenRepositoryInterface
            new ScopeRepository($this->settings['scopes']),                  // instance of ScopeRepositoryInterface
            $pathToPrivateKey,    // path to private key
            $encryptionKey      // encryption key
        );

        $grant = new PasswordGrant(
            new UserRepository(),           // instance of UserRepositoryInterface
            new RefreshTokenRepository()    // instance of RefreshTokenRepositoryInterface
        );
        $grant->setRefreshTokenTTL(new \DateInterval($this->settings['refreshTokenTTL'])); // refresh tokens will expire after 1 month

        // Enable the password grant on the server with a token TTL of 1 hour
        $server->enableGrantType(
            $grant,
            new \DateInterval($this->settings['accessTokenTTL']) // access tokens will expire after 1 hour
        );

        $request = new \TYPO3\CMS\Core\Http\ServerRequest(null, 'POST');
        $request = $request->withParsedBody([
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'username' => $userName,
            'password' => $userPassword,
            'scope' => $scope,
            'grant_type' => $grantType,
        ]);
        $response = new \TYPO3\CMS\Core\Http\Response();

        try {
            // Try to respond to the access token request
            $server->respondToAccessTokenRequest($request, $response);
            $this->response->setStatus($response->getStatusCode());
            return $response->getBody();
        } catch (OAuthServerException $exception) {
            // All instances of OAuthServerException can be converted to a PSR-7 response
            $exception->generateHttpResponse($response);
            $this->response->setStatus(500);
            return $response->getBody();
        }
    }

    /**
curl -X "POST" "https://my.ddev.site/?tx_oauth2server_oauth2[action]=refresh&type=1586350745" \
-H "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "tx_oauth2server_oauth2[clientId]=MY-CLIENT-ID" \
--data-urlencode "tx_oauth2server_oauth2[clientSecret]=MY-CLIENT-SECRET" \
--data-urlencode "tx_oauth2server_oauth2[refreshToken]=MY-REFRESH-TOKEN" \
--data-urlencode "tx_oauth2server_oauth2[scope]=MY-SCOPE-1 MY-SCOPE-2"
     *
curl -X "POST" "https://my.ddev.site/oauth2/access_token/refresh/MY-CLIENT-ID/MY-SCOPE-1+MY-SCOPE-2/1586350745" \
-H "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "tx_oauth2server_oauth2[clientSecret]=MY-CLIENT-SECRET" \
--data-urlencode "tx_oauth2server_oauth2[refreshToken]=MY-REFRESH-TOKEN"
     *
     * @param string $clientId
     * @param string $clientSecret
     * @param string $refreshToken
     * @param string $scope
     */
    public function refreshAction($clientId, $clientSecret, $refreshToken, $scope){
        $grantType = 'refresh_token';
        $extSettings = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('oauth2_server');
        $pathToPrivateKey = 'file://' . PathUtility::getAbsolutePathOfRelativeReferencedFileOrPath(Environment::getProjectPath(). '/', $extSettings['pathToPrivateKey']);
        $encryptionKey = $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'];
        //var_dump($clientId, $clientSecret, $userName, $userPassword, $scope, $grantType, $pathToPrivateKey, $encryptionKey);

        // Setup the authorization server
        $server = new AuthorizationServer(
            new ClientRepository(),                 // instance of ClientRepositoryInterface
            new AccessTokenRepository(),            // instance of AccessTokenRepositoryInterface
            new ScopeRepository($this->settings['scopes']),                  // instance of ScopeRepositoryInterface
            $pathToPrivateKey,    // path to private key
            $encryptionKey      // encryption key
        );

        // Enable the refresh token grant on the server
        $grant = new RefreshTokenGrant(
            new RefreshTokenRepository()    // instance of RefreshTokenRepositoryInterface
        );
        $grant->setRefreshTokenTTL(new \DateInterval($this->settings['refreshTokenTTL'])); // refresh tokens will expire after 1 month

        // Enable the password grant on the server with a token TTL of 1 hour
        $server->enableGrantType(
            $grant,
            new \DateInterval($this->settings['accessTokenTTL']) // access tokens will expire after 1 hour
        );

        $request = new \TYPO3\CMS\Core\Http\ServerRequest(null, 'POST');
        $request = $request->withParsedBody([
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'refresh_token' => $refreshToken,
            'scope' => $scope,
            'grant_type' => $grantType,
        ]);
        $response = new \TYPO3\CMS\Core\Http\Response();

        try {
            // Try to respond to the access token request
            $server->respondToAccessTokenRequest($request, $response);
            $this->response->setStatus($response->getStatusCode());
            return $response->getBody();
        } catch (OAuthServerException $exception) {
            // All instances of OAuthServerException can be converted to a PSR-7 response
            $exception->generateHttpResponse($response);
            $this->response->setStatus(500);
            return $response->getBody();
        }
    }
}
