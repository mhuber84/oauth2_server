<?php
declare(strict_types = 1);
namespace Mhuber84\Oauth2Server\Middleware;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResourceServer;
use Mhuber84\Oauth2Server\Domain\Oauth2Repositories\AccessTokenRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Authentication\LoginType;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;

/**
 * This middleware triggers the FrontendUserAuthentication by setting the POST parameter "logintype"
 */
class Oauth2Authenticator implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Workaround for proxies wich remove authorization headers ( IONOS :-( )
        $postVars = $request->getParsedBody() ?: [];
        if ($request->hasHeader('authorization') === false && isset($postVars['tx_oauth2server_oauth2']['authorization'])) {
            $request = $request->withHeader('authorization', $postVars['tx_oauth2server_oauth2']['authorization'] );
        }
        if ($request->hasHeader('authorization') === false) {
            return $handler->handle($request);
        }
        if (substr($request->getHeader('authorization')[0], 0, 7) !== 'Bearer ') {
            return $handler->handle($request);
        }

        $extSettings = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('oauth2_server');
        $pathToPublicKey = 'file://' . PathUtility::getAbsolutePathOfRelativeReferencedFileOrPath(Environment::getProjectPath(). '/', $extSettings['pathToPublicKey']);

        $server = new ResourceServer(
            new AccessTokenRepository(),
            $pathToPublicKey
        );

        try {
            $request = $server->validateAuthenticatedRequest($request);
        } catch (OAuthServerException $exception){
            $response = new JsonResponse();
            return $exception->generateHttpResponse($response);
        }

        $userUid = $request->getAttribute('oauth_user_id');
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('fe_users');
        $user = $queryBuilder
            ->select('*')
            ->from('fe_users')
            ->where( $queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($userUid, Connection::PARAM_INT)))
            ->execute()
            ->fetch();

        $oldPostVars = $request->getParsedBody() ?: [];
        $request = $request
            ->withParsedBody(array_merge($oldPostVars, [
                'logintype' => LoginType::LOGIN,
                'user' => $user['username'],
                'pass' => 'dummy',
                'pid' => $extSettings['userPid'],
            ]));

        $GLOBALS['TYPO3_REQUEST'] = $request;

        // we have to set $_POST directly, because
        // \TYPO3\CMS\Core\Authentication\AbstractUserAuthentication::getLoginFormData uses GeneralUtility::_GP
        $_POST = array_merge($_POST, [
            'logintype' => LoginType::LOGIN,
            'user' => $user['username'],
            'pass' => 'dummy',
            'pid' => $extSettings['userPid'],
        ]);

        return $handler->handle($request);
    }
}
