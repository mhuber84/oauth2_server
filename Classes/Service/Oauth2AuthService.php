<?php
namespace Mhuber84\Oauth2Server\Service;

use TYPO3\CMS\Core\Authentication\AbstractAuthenticationService;
use TYPO3\CMS\Core\Http\ServerRequest;

class Oauth2AuthService extends AbstractAuthenticationService {
    /**
     * @param array $user User data
     * @return int Authentication status code, one of 0, 100, 200
     */
    public function authUser(array $user): int
    {
        /** @var ServerRequest $request */
        $request = $GLOBALS['TYPO3_REQUEST'];
        if(
            $request->hasHeader('authorization')
            && substr($request->getHeader('authorization')[0], 0, 7) === 'Bearer '
        ){
            if(
                strlen($request->getAttribute('oauth_access_token_id')) > 0
                && strlen($request->getAttribute('oauth_client_id')) > 0
                && (int) $request->getAttribute('oauth_user_id') > 0
                && !empty($request->getAttribute('oauth_scopes'))
                && (int) $user['uid'] === (int) $request->getAttribute('oauth_user_id')
            ){
                // Validated without password by Oauth2Authenticator.
                return 200;
            }

            // This should never happen. Oauth2Authenticator should have thrown a OAuthServerException before.
            return 0;
        }

        // Check other services. Next servoce is default TYPO3 AuthenticationService.
        return 100;
    }
}