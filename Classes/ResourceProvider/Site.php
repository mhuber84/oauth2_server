<?php
namespace Mhuber84\Oauth2Server\ResourceProvider;

class Site {

    /**
     * @param array $conf
     * @return array
     */
    public function get($conf = []){
        return [
            'uid' => $GLOBALS['TSFE']->page['uid'],
            'title' => $GLOBALS['TSFE']->page['title'],
        ];
    }
}