<?php
namespace Mhuber84\Oauth2Server\ResourceProvider;

class User {

    /**
     * @param array $conf
     * @return array
     */
    public function getBasic($conf = []){
        return [
            'uid' => $GLOBALS['TSFE']->fe_user->user['uid'],
            'username' => $GLOBALS['TSFE']->fe_user->user['username'],
        ];
    }

    /**
     * @param array $conf
     * @return array
     */
    public function getDefault($conf = []){
        return array_merge(
            $this->getBasic(),
            [
                'first_name' => $GLOBALS['TSFE']->fe_user->user['first_name'],
                'last_name' => $GLOBALS['TSFE']->fe_user->user['last_name'],
                'email' => $GLOBALS['TSFE']->fe_user->user['email'],
            ]
        );
    }
}