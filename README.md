# OAuth2 Server TYPO3 Extension

This is a TYPO3 extension. Use TYPO3 as an OAuth2 Server. Based on https://oauth2.thephpleague.com

## What does it do?

With this extension you can use your TYPO3 as an Oauth2 Server.

## Installation

1. Install the extension by using composer: `composer req mhuber84/oauth2-server`.
2. Create a private and a public key and set the path in the extension manager: https://oauth2.thephpleague.com/installation/
3. Set the SysFolder's uid with your fe_users in the extension manager.
4. Include the static TypoScript.
5. Import the routing configuration into your site configuration:
```
imports:
  - { resource: 'EXT:oauth2_server/Configuration/Routes/Default.yaml' }
```  
6. Create a "Client" record on your root page and select a scope. The client is the third party app. A scope defines the data, the client
is authorized to get (Resource Provider).

## Usage

### Authenticate and authorize user

To authenticate your user, currently only the Password Grant is implemented.

In your third party app create a form with username and password. Make a call like this to authenticate the user:
```
curl -X "POST" "https://my.ddev.site/oauth2/access_token/get/MY-CLIENT-ID/MY-USER-NAME/MY-SCOPE-1+MY-SCOPE-2/1586350745" \
-H "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "tx_oauth2server_oauth2[clientSecret]=MY-CLIENT-SECRET" \
--data-urlencode "tx_oauth2server_oauth2[userPassword]=MY-USER-PASSWORD"
```
You will get back an access token and a refresh token. You will need the access token later.

### Get data from TYPO3 (Resource Providers)

If you need data from TYPO3 (f.e. the address from the user) in your third party app, you need resource providers. The extension has
some basic resource provider and you can register more providers (TCA & TypoScript).

```
curl -X "POST" "https://my.ddev.site/resource/get/1586350745" \
-H "Authorization: Bearer MY-ACCESS-TOKEN"
```

## TODO

Implement other Grants

## Author

<a href="https://marco-huber.de">Marco Huber</a>, <a href="https://twitter.com/mhuber84">@mhuber84</a>, mail@marco-huber.de

Feel free to contribute and send in pull requests or create an issue.
