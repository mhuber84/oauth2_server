<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin (
    'Mhuber84.Oauth2Server',
    'Oauth2',
    [
        'Authentication' => 'login,refresh',
        'Resource' => 'get',
    ],
    [
        'Authentication' => 'login,refresh',
        'Resource' => 'get',
    ]
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addService(
    'oauth2_server',
    'auth',
    \Mhuber84\Oauth2Server\Service\Oauth2AuthService::class,
    [
        'title' => 'Oauth2 authentication',
        'description' => 'Authenticate user by user uid and oauth access token.',
        'subtype' => 'authUserFE',
        'available' => true,
        'priority' => 60,
        'quality' => 50,
        'os' => '',
        'exec' => '',
        'className' => \Mhuber84\Oauth2Server\Service\Oauth2AuthService::class,
    ]
);

$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_oauth2server_oauth2[clientId]';
$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_oauth2server_oauth2[userName]';
$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_oauth2server_oauth2[scope]';