<?php
return [
    'frontend' => [
        'mhuber84/oauth2-server/authentication' => [
            'target' => \Mhuber84\Oauth2Server\Middleware\Oauth2Authenticator::class,
            'after' => [
                'typo3/cms-frontend/tsfe',
            ],
            'before' => [
                'typo3/cms-frontend/authentication',
            ]
        ],
    ]
];
