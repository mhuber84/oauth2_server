<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:refreshtoken',
        'label' => 'token',
        'iconfile' => 'EXT:oauth2_server/Resources/Public/Icons/refreshtoken.png',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'sortby' => 'sorting',
        'descriptionColumn' => 'rowDescription',
        'editlock' => 'editlock',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'translationSource' => 'l10n_source',
        'rootLevel' => true,
        'hideTable' => true,
    ],
    'interface' => [
        'showRecordFieldList' => 'token,expiry,access_token,user,client,revoked,rowDescription,starttime,endtime,fe_group,editlock'
    ],
    'palettes' => [
    ],
    'types' => [
        '0' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,token,expiry,access_token,revoked,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,rowDescription,hidden,starttime,endtime,fe_group,editlock',
        ],
    ],
    'columns' => [
        'hidden' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['disabled']],
        'starttime' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['starttime']],
        'endtime' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['endtime']],
        'fe_group' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['fe_group']],
        'rowDescription' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['descriptionColumn']],
        'editlock' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['editlock']],
        'token' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:refreshtoken.token',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required',
            ],
        ],
        'expiry' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:refreshtoken.expiry',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required,datetime',
            ],
        ],
        'access_token' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:refreshtoken.access_token',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_oauth2server_domain_model_accesstoken',
                'minitems' => 1,
                'maxitems' => 1,
            ],
        ],
        'revoked' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:refreshtoken.revoked',
            'config' => [
                'type' => 'check',
            ],
        ],
    ],
];