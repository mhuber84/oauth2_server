<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client',
        'label' => 'name',
        'label_alt' => 'auth_id',
        'iconfile' => 'EXT:oauth2_server/Resources/Public/Icons/client.png',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'sortby' => 'sorting',
        'descriptionColumn' => 'rowDescription',
        'editlock' => 'editlock',
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'translationSource' => 'l10n_source',
        'rootLevel' => true,
    ],
    'interface' => [
        'showRecordFieldList' => 'auth_id,auth_secret,name,redirect_uri,confidential,scopes,rowDescription,starttime,endtime,fe_group,editlock'
    ],
    'palettes' => [
    ],
    'types' => [
        '0' => [
            'showitem' => '--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,auth_id,auth_secret,name,redirect_uri,confidential,scopes,--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,rowDescription,hidden,starttime,endtime,fe_group,editlock',
        ],
    ],
    'columns' => [
        'hidden' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['disabled']],
        'starttime' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['starttime']],
        'endtime' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['endtime']],
        'fe_group' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['enablecolumns']['fe_group']],
        'rowDescription' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['descriptionColumn']],
        'editlock' => $GLOBALS['TCA']['tt_content']['columns'][$GLOBALS['TCA']['tt_content']['ctrl']['editlock']],
        'auth_id' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.auth_id',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required,unique',
            ],
        ],
        'auth_secret' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.auth_secret',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required,password,saltedPassword',
            ],
        ],
        'name' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.name',
            'config' => [
                'type' => 'input',
                'eval' => 'trim,required',
            ],
        ],
        'redirect_uri' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.redirect_uri',
            'config' => [
                'type' => 'input',
                'eval' => 'trim',
            ],
        ],
        'confidential' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.confidential',
            'config' => [
                'type' => 'check',
            ],
        ],
        'scopes' => [
            'label' => 'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.scopes',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'items' => [
                    ['LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.scopes.user-basic', 'user-basic'],
                    ['LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.scopes.user-default', 'user-default'],
                    ['LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:client.scopes.site', 'site'],
                ],
            ],
        ],
    ],
];