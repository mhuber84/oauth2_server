<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'oauth2_server',
    'Oauth2',
    'LLL:EXT:oauth2_server/Resources/Private/Language/locallang.xlf:tt_content.list_type.oauth2_server.oauth2-plugin'
);