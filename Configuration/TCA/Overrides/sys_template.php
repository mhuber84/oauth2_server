<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('oauth2_server', 'Configuration/TypoScript', 'OAuth2 Server');
